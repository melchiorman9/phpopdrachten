<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 09/10/2018
 * Time: 22:17
 */


$conn = mysqli_connect('127.0.0.1', 'root', '', 'phpopdrachten', '3306');
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT winkel.Naam, winkel.Plaats, count(bestelling.Bestelnr) as totaal FROM bestelling, winkel WHERE bestelling.Wcode = winkel.Wcode";

$res = mysqli_query($conn, $sql);

if ($res)
    echo 'geen fout';
else
    echo mysqli_error($conn);

?>

<table border="1">
    <tr>
        <th colspan="3">Overzicht aantal bestellingen per winkel</th>
    </tr>
    <tbody>
        <?php
        foreach($tableData as $tableDataStuk):

            echo "<tr><td>".$tableDataStuk["naam"]."</td><td>".$tableDataStuk["plaats"]."</td><td>".$tableDataStuk["totaal"]."</td></tr>";

        endforeach;
        ?>
    </tbody>
</table>
