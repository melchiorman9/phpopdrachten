<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 28/09/2018
 * Time: 10:48
 */
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $num1 = $_POST["num1"];
    $num2 = $_POST["num2"];
    $action = $_POST["handeling"];

    if (is_numeric($num1) and is_numeric($num2)) {
        if ($num1 == 0 or $num2 == 0) {
            if ($action == "delenDoor") {
                die ("<a href='5.php'>Delen door 0 mag niet!</a>");
            }
        }
    }

    function determineHandeling($action){
        switch ($action){
            case 'optellen':
                return $action = '+';
            case 'aftrekken':
                return $action = '-';
            case 'vermenigvuldigen':
                return $action = '*';
            case 'delenDoor':
                return $action = '/';
            default:
                return $action = '+';
        }
    }

    echo  "De ingevoerde berekening was: $num1".determineHandeling($action)."$num2 = ";
    eval('echo('.$num1.determineHandeling($action).$num2.');');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="" method="post">
    <label for="1">Getal 1:
        <input id="1" type="number" name="num1">
    </label>
    <br>
    <label for="2">Getal 2:
        <input id="2" type="number" name="num2">
    </label>
    <br>
    <label for="radio1">
        <input id="radio1" name="handeling" value="optellen" type="radio">
        Optellen
    </label>
    <label for="radio2">
        <input id="radio2" name="handeling" value="aftrekken" type="radio">
        Aftrekken
    </label>
    <label for="radio3">
        <input id="radio3" name="handeling" value="vermenigvuldigen" type="radio">
        Vermenigvuldigen
    </label>
    <label for="radio4">
        <input id="radio4" name="handeling" value="delenDoor" type="radio">
        Delen door
    </label>
    <hr>

    <input type="submit" value="Bereken">
    <input type="reset" value="Velden leegmaken">
</form>


</body>
</html>
