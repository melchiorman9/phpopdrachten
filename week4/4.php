<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 27/09/2018
 * Time: 22:43
 */

if($_SERVER["REQUEST_METHOD"]=="POST"){
    $props[0] = $_POST['height'];
    $props[1] = $_POST['thicc'];
    $props[2] = $_POST['color'];
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form method="post" action="">
    <label for="1">
        Vul hier de Hoogte van de lijn in:
        <input id="1" name="height" type="number">
        px
    </label> <br>
    <label for="2">
        Vul hier de dikte van de lijn in:
        <input id="2" name="thicc" type="number">
        px
    </label> <br>
    <label for="3">
        Vul hier de kleur van de lijn in:
        <input id="3" name="color" type="text">
        px
    </label> <br>
    <input type="submit">
</form>

<div class="lijn" style="border-radius:50px; <?php if(isset($props)){echo " height:$props[0]px; width:$props[1]px; background-color: $props[2];";}?>">

</div>

</body>
</html>
