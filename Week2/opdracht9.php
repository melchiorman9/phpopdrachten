<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 15/09/2018
 * Time: 17:26
 */
//temperaturen in string
$maand_temp = "22, 24, 25, 22, 21, 20, 18, 22, 23, 25, 24, 22, 21, 19, 18, 16, 17, 18, 22, 23, 26, 27, 23, 22, 20, 18, 18, 19, 20, 22, 24";
//explode de string naar array
$maand_temp = explode(',',$maand_temp);
//Pak het gemiddelde
$average = round(array_sum($maand_temp)/count($maand_temp), 1);
//Counter maken
$c = 3;
//Bepaalde graden ophalen 3x
while($c > 0){
    //Haal hoogste en laagste graden op voor deze iteratie. in de eerste iteratie zijn dit de absolute hoogste en laagste.
    $lowest = min($maand_temp);
    $highest = max($maand_temp);
    //Plekken van de zojuist gevonden waarden vinden
    $l = array_keys($maand_temp,$lowest);
    $h = array_keys($maand_temp,$highest);
    //Plekken klaar maken voor gebruik
    $l = $l[0];
    $h = $h[0];
    //Nummers toevoegen aan array voor gebruik buiten de while loop.
    $highests[] = $highest;
    $lowests[] = $lowest;
    //Verwijderen uit de array zodat de volgende gevonden kan worden
    unset($maand_temp[$h]);
    unset($maand_temp[$l]);
    //en even een klein stukje van de counter eraf, ik wil ook nog naar huis ;)
    $c--;
}
//Omdat de array max de hoogste zoekt, en we dus naar beneden werken staan ze niet meer mooi op volgorde,
//dit verbeteren we met array reverse, maar ik had net zo goed sort kunnen gebruiken.
$highests = array_reverse($highests);

//En de echo's
echo "De gemiddelde temperatuur was: $average <br>";
echo "De laagste temperaturen waren: $lowests[0], $lowests[1], $lowests[2] <br>";
echo "De hoogste temperaturen waren: $highests[0], $highests[1], $highests[2]";