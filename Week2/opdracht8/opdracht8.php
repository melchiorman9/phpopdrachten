<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 15/09/2018
 * Time: 17:22
 */
$arrayNames = ["Death From Above","The War On Drugs","DMA's","Dropkick Murphy's"];
$arrayPics = ["/death_from_above.jpg","/thewarondrugs.jpg","/dmas.jpg","/dropkick_murphys.jpg"];
$multi = [$arrayNames,$arrayPics];

echo "<table border=\"1\">
        <caption>Line Up Lowlands 2018</caption>";
echo "<tr>";
foreach ($multi[1] as $pic){
    echo "<td><img style='width:150px;' src='./images/$pic' alt=''></td>";
}
echo "</tr><tr>";
foreach ($multi[0] as $name) {
    echo "<td style='text-align: center'><b>$name</b></td>";
}
echo "</tr>";
