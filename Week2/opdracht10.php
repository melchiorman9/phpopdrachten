<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 15/09/2018
 * Time: 19:28
 */

$game1 = [
    "gameName"=>"Regenwormen",
    "gamePlayers"=>"2 tot 7",
    "gameAge"=>8,
    "gamePrice"=>14.99
];
$game2 = [
    "gameName"=>"30 Seconds",
    "gamePlayers"=>"3 tot 24",
    "gameAge"=> 12,
    "gamePrice"=> 29.95
];
$game3 = [
    "gameName"=>"Ticket to Ride Europe",
    "gamePlayers"=>"3 tot 5",
    "gameAge"=>8,
    "gamePrice"=> 34.95
];

$games = [$game1,$game2,$game3];

foreach ($games as $gameProperty => $gameValue ){
    echo 'Het spel '.$gameValue["gameName"]. ' kan gespeeld worden met '.$gameValue["gamePlayers"]. ' spelers, vanaf de leeftijd '.$gameValue['gameAge'].' en de prijs is '.$gameValue['gamePrice'].'<br>';
}

echo "<br>Het spel ".$game1["gameName"]." kost ".$game1["gamePrice"];
echo "<br>Het spel dat ".$game3["gamePrice"]." kost is ".$game3["gameName"];
