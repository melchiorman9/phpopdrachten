<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 17/09/2018
 * Time: 14:45
 */
$rows = 15;
$columns = 15;

echo "<table border=\"1\">";
for ($r=1;$r<=$rows;$r++){
    if ($r == 1){
        echo "<tr style='font-weight: bold;'>";
    }else{
        echo "<tr>";
    }
    for($c=1;$c<=$columns;$c++){
        if ($c == 1){
            echo "<td style='font-weight: bold;'>".$c*$r."</td>";
        }else{
            echo "<td>".$c*$r."</td>";
        }

    }
    echo "</tr>";
}
echo "</table>";

echo "<img src='opdracht11.PNG'>"
?>