<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 15/09/2018
 * Time: 13:45
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulier opdracht 14</title>
</head>
<body>

<form action="o4_chk_fruit.php" method="post">
    <select name="fruit" id="">
        <option value="Banaan">Banaan</option>
        <option value="Appel">Appel</option>
        <option value="Peer">Peer</option>
    </select>
    <input type="submit">
</form>
<img src='opdracht4.PNG'/>
</body>
</html>
