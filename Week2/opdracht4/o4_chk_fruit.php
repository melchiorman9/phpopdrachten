<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 15/09/2018
 * Time: 14:14
 */
echo "<h1>Een spreekwoord bij de gekozen vrucht:</h1>";
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    if ($_POST["fruit"]){
        switch ($_POST["fruit"]){
            case "Banaan":
                echo "Gaan met die banaan!";
                break;
            case "Appel":
                echo "De appel valt niet ver van de boom";
                break;
            case "Peer":
                echo "Appels met peren vergelijken";
                break;
            default:
                echo "Er is iets mis gegaan bij het selecteren van het fruit";
                break;
        }
    }else{
        echo "You broke it";
    }
}