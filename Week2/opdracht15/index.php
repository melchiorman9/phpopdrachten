<style>
    table{
        width: 270px;
        height: 270px;
        border: solid black;
    }

    td {
        height:30px;
        width: 30px;
    }

    .black{
        background-color: black;
    }
</style>
<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 17/09/2018
 * Time: 20:51
 */
echo "<table>";
for ($i=0;$i<8;$i++){
    echo "<tr>";

    for ($c=0;$c<8;$c++){
        $total = $i+$c;
        if ($total%2 ==0){
            echo "<td id=\"$i $total\" class='black'></td>";
        }else{
            echo "<td id=\"$i $total\"></td>";
        }

    }
    echo "</tr>";
}
echo "</table>";
