<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 15/09/2018
 * Time: 16:38
 */
//nummer aanmaken
$num = rand(0,20);
//factorial aanmaken
$factorial = 1;
//Voor elke stap tussen het nummer en 1 (1 hoeft niet want 1x1 maakt alleen een extra stap)
for ($i=$num;$i>1; $i--){
    //Factorial vermenigvuldigen met het nummer waar we zijn
    $factorial = $factorial * $i;
}
echo  "Factorial of $num is $factorial";