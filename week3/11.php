<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 26/09/2018
 * Time: 12:56
 */

function reverseString($str){
    $len = strlen($str)-1;
    $revStr = '';
    while ($len >= 0){
        $revStr .= $str[$len];
        $len -=1;
    }
    return $revStr;
}
function checkPalindrome($string){
    if (reverseString($string) === $string){
        return "$string is een palindroom";
    }else{
        return "$string is geen palindroom";
    }
}

echo  checkPalindrome('racecar');