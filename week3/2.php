<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 19/09/2018
 * Time: 13:05
 */

function faculteit($input){
    //factorial aanmaken
    $factorial = 1;
    //Voor elke stap tussen het nummer en 1 (1 hoeft niet want 1x1 maakt alleen een extra stap)
    for ($i=$input;$i>1; $i--){
        //Factorial vermenigvuldigen met het nummer waar we zijn
        $factorial = $factorial * $i;
    }
    return $factorial;
}

echo faculteit(4);