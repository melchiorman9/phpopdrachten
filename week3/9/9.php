<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 24/09/2018
 * Time: 11:59
 */

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    function dobbelsteen(){
        return rand(1,6);
    }
    $result = dobbelsteen();
    echo '<img src="images/'.$result.'.gif"/>';
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dobbelsteen</title>
</head>
<body>
    <form action="" method="POST">
        <input type="submit" name="submit" value="Werp dobbelsteen">
    </form>
</body>
</html>

