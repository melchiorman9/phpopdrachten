<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 19/09/2018
 * Time: 13:11
 */

$getal = 0;

function getalModuloNul($getal){
    $count = $getal-1;
    while($count > 1){
        if ($getal % $count == 0){
            //getal is deelbaar door iets anders dan 1 dus geen priem
            return true;
        }else{
            //getal is niet deelbaar door een van de getallen onder hem, en is dus misschien priem
            $count--;
        }
    }
}


function isPriem($getal){
    if ($getal == 1 or $getal == 0){
        return "$getal is geen priemgetal";
    }
    if ($getal == 2){
        return "$getal is een priemgetal";
    }

    if(getalModuloNul($getal)){
        return "$getal is geen priemgetal";
    }else{
        return "$getal is een priemgetal";
    }
}

for($i=0;$i<150;$i++){
    echo isPriem($i)."<br>";
}

