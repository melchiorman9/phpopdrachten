<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 21/09/2018
 * Time: 13:45
 */

function berekenEindBedrag($prijs, $btw)
{
    $taxPrice = $prijs / 100 * $btw;
    $total = $prijs + $taxPrice;

    return [$prijs, $taxPrice, $total];
}

$receipt = berekenEindBedrag(12,21);

echo "Prijs van het artikel is: ".$receipt[0]."<br>";
echo "Prijs van de belasting is: ".$receipt[1]."<br>";
echo "Totaalprijs is: ".$receipt[2]."<br>";