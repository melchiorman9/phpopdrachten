<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 21/09/2018
 * Time: 13:08
 */

function takeRandomAmountOfArgumentsAndCalculateTheAverage(){
    $total = NULL;
    $args = func_get_args();
    $argCount= count($args);
    if ($argCount >= 1){
        foreach ($args as $arg){
            $total += $arg;
        }
        return $average = $total/$argCount;
    }else{
        return "There was no argument to the function";
    }
}
echo takeRandomAmountOfArgumentsAndCalculateTheAverage();