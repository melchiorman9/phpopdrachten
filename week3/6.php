<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 19/09/2018
 * Time: 14:56
 */

$test = [1, 2, 3];
$testWaarden = [2,6];
function checkArray($needle, $haystack)
{
    try {
        foreach ($haystack as $testPart) {
            foreach ($needle as $testWaarde) {
                if ($testPart == $testWaarde) {
                    return true;
                }
            }
        }
        return false;
    } catch (Exception $exception){
        echo "Caught exception: ".$exception->getMessage();
    }
}
echo checkArray($testWaarden, $test);