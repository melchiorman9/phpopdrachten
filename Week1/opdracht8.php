<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 12/09/2018
 * Time: 20:28
 */

function calculate_circumference($number){
    return 2 * 3.14 * $number;
}
function calculate_surface($number){
    return 3.14 * $number * $number;
}
function run_calculation($num){
    echo "De omtrek van een cirkel met een straal van $num centimeter is ".calculate_circumference($num)." cm <br>";
    echo "De oppervlakte van een cirkel met een straal van $num centimeter is ".calculate_surface($num)." cm<sup>2</sup><br>";
}
run_calculation(7);

