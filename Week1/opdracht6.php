<?php
/**
 * Created by PhpStorm.
 * User: Melchior
 * Date: 12/09/2018
 * Time: 20:20
 */

//Number
$mijnNummer = -7;
//Absolute value of the number
$absoluteValue = abs($mijnNummer);
echo ("Absolute waarde is: ".$absoluteValue."<br>");
//Absolute value times 4
$absoluteValue = $absoluteValue*4;
echo ("Absolute waarde keer 4 is: ".$absoluteValue."<br>");
//Square root of the value
$absoluteValue = sqrt($absoluteValue);
echo ("Wortel van absolute waarde is: ".$absoluteValue."<br>");
//Value ceiled
$absoluteValue = ceil($absoluteValue);
echo ("Afgeronde waarde is: ".$absoluteValue."<br>");