<?php
/**
 * Created by PhpStorm.
 * User: melle
 * Date: 13/09/2018
 * Time: 11:50
 */
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>Opdracht15</title>
</head>
<body>

<h3 class="heading">Geboren op 9 juni</h3>

<p><b>Peter de Grote</b> Russische tsaar (1672)</p>
<p><b>Johnny Depp</b> Amerikaans acteur (1963)</p>
<p><b>Dione de Graaf</b> Nederlands televisiepresentatrice (1969)</p>
<p><b>Matthew Bellamy</b> Brits zanger, gitarist en toetsenist (Muse) (1978)</p>
<p><b>Wesley Sneijder</b> Nederlands voetballer (1984)</p>
<p><b>Matthias Mayer</b> Oostenrijks alpineskiër (goud sotchi) (1990)</p>

</body>
</html>
